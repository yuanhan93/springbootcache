package com.just.springbootcache.controller;

import com.just.springbootcache.domain.Person;
import com.just.springbootcache.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PeopleController {
    @Autowired
    private PersonService personService;
    @PostMapping("/put")
    public Person put(@RequestBody Person person){
        Person p=personService.save(person);
        return p;
    }
    @PostMapping("/able")
    public Person able(@RequestBody Person person){
        Person p=personService.findOne(person);
        return p;
    }
    @GetMapping("/evict")
    public String evict(Long id){
        personService.remove(id);
        return "ok";
    }

    @GetMapping("/conditionFind")
    public Person conditionFindById(Long id){
        return personService.conditionFindById(id);
    }
    @GetMapping("/removeAll")
    public String removeAll(){
        personService.removeAll();
        return "all is well";
    }

}
