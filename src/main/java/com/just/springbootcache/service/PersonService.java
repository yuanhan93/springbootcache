package com.just.springbootcache.service;

import com.just.springbootcache.domain.Person;

public interface PersonService {

    Person save(Person person);

    void remove(Long id);

    Person findOne(Person person);

    Person conditionFindById(Long id);

    void removeAll();

}
